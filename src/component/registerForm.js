import { useHistory } from 'react-router-dom';
import { useState } from 'react'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
const useStyles = makeStyles((theme) => ({
	paper: {
		margin: theme.spacing(8, 4),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

export default function RegisterForm() {
  const [nameError,setNameError] = useState(false)
  const [lastError,setLastError] = useState(false)
  const [emailError,setemailError] = useState(false)
	const [passError, setPassError] = useState(false)
  const [confPassError,setconfPassError] = useState(false)
  const [mobileError,setMobileError] = useState(false)
	const [error ,setError] = useState(false)
   const history = useHistory()
   const [showAlert, setShowAlert] = useState(false)
  const [credentials, setCredentials] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
    mobileNo: "",
    isAdmin: false
  })
  const [isLoading, setIsLoading] = useState(false)
  const handleSubmit = e => {
    e.preventDefault()
    if(credentials.firstName == ''){
      setNameError(true)
    }
    if(credentials.lastName == ''){
      setLastError(true)
    }
    if(credentials.email == ''){
			setemailError(true)
		}
		if(credentials.password == ''){
			setPassError(true)
		}
    if(credentials.confirmPassword == ''){
			setconfPassError(true)
		}
    if(credentials.mobileNo == ''){
			setMobileError(true)
		}
    else{
    setIsLoading(true)
    fetch('https://blooming-basin-05163.herokuapp.com/api/users/', {
      method: "POST",
      body: JSON.stringify(credentials),
      headers:{
        "Content-Type" : "application/json"
      }

    })
    .then( res => {
			console.log(res)
			setIsLoading(false)
      if(credentials.password != credentials.confirmPassword){
        setPassError(true)
			  setconfPassError(true)
        setError(true)
        
      }else{
        if (res.status === 200) {
          alert("Register successful")
          history.push("/")
          return res.json()
          
        } else {
          
        }
      }
		})
  }
}
  const handleChange = e => {
      setNameError(false)  
      setLastError(false)
			setemailError(false)		
			setPassError(false)
			setconfPassError(false)
			setMobileError(false)		
    setCredentials({
      ...credentials,
      [e.target.id]: e.target.value
      
    })
  }

	const classes = useStyles()
    return (
    
		<div className={classes.paper}>
		<Typography component="h1" variant="h5">
			Register
		</Typography>
		<form className={classes.form} onSubmit={handleSubmit} noValidate>
			<TextField
				variant="outlined"
				margin="normal"
				required
				fullWidth
				id="firstName"
				onChange={handleChange}
				value={credentials.firstName}
				label="First Name"
				name="firstName"
				type="text"
				autoFocus
				error={nameError}
				
			/>
			<TextField
				variant="outlined"
				margin="normal"
				required
				fullWidth
				name="lastName"
				label="Last Name"
				type="text"
				id="lastName"
				onChange={handleChange}
				value={credentials.lastName}
				error ={lastError}
			/>
      <TextField
				variant="outlined"
				margin="normal"
				required
				fullWidth
				name="email"
				label="Email Address"
				type="email"
				id="email"
				onChange={handleChange}
				value={credentials.email}
				error ={emailError}
			/>
      <TextField
				variant="outlined"
				margin="normal"
				required
				fullWidth
				name="password"
				label="Password"
				type="password"
				id="password"
				onChange={handleChange}
				value={credentials.password}
				error ={passError}
			/>
      <TextField
				variant="outlined"
				margin="normal"
				required
				fullWidth
				name="confirmPassword"
				label="Comfirm Password"
				type="password"
				id="confirmPassword"
				onChange={handleChange}
				value={credentials.confirmPassword}
				error ={confPassError}
			/>
      <TextField
				variant="outlined"
				margin="normal"
				required
				fullWidth
				name="mobilNumber"
				label="Mobile"
				type="number"
				id="mobileNo"
				onChange={handleChange}
				value={credentials.password}
				error ={mobileError}
			/>
			{error?
			<Alert severity="error">Password doesn't match</Alert> : ''
			}
			<Button
				type="submit"
				fullWidth
				variant="contained"
				color="primary"
				className={classes.submit}
			>
				Sign Up
			</Button>				
				<Grid item>					
				</Grid>
		</form>
	</div>
    // <Col className="mx-auto col-12">
    //   <Form onSubmit={handleSubmit}>
    //   <h1 style={{textAlign:"center"}}>Register</h1>
    //     <Form.Group controlId="firstName">
    //       <Form.Label>First Name:</Form.Label>
    //       <Form.Control type="text" onChange={handleChange}
    //         value={credentials.firstName} />
    //     </Form.Group>
    //     <Form.Group controlId="lastName">
    //       <Form.Label>Last Name:</Form.Label>
    //       <Form.Control type="text" onChange={handleChange}
    //         value={credentials.lastName} />
    //     </Form.Group>
    //     <Form.Group controlId="email">
    //       <Form.Label>Email:</Form.Label>
    //       <Form.Control type="email" onChange={handleChange}
    //         value={credentials.email} />
    //     </Form.Group>
    //     <Form.Group controlId="password">
    //       <Form.Label>Password:</Form.Label>
    //       <Form.Control type="password" onChange={handleChange}
    //         value={credentials.password} />
    //     </Form.Group>
    //     <Form.Group controlId="confirmPassword">
    //       <Form.Label>Confirm Password:</Form.Label>
    //       <Form.Control type="password" onChange={handleChange}
    //         value={credentials.confirmPassword} />
    //     </Form.Group>
    //     <Form.Group controlId="mobileNo">
    //       <Form.Label>Mobile No:</Form.Label>
    //       <Form.Control type="text" onChange={handleChange}
    //         value={credentials.mobileNo} />
    //         {showAlert ?
		// 		<Alert variant="danger">
		// 			Password doesnt match
		// 		</Alert>:""
		// 			}	
    //     </Form.Group>
    //     {isLoading ?
    //       <Button type="submit" disabled>Register</Button>
    //       : <Button type="submit" >Register</Button>
    //     }
    //   </Form>
    // </Col>
  )
}