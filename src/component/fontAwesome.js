import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheckSquare, faCoffee,faPlus,faEdit,faTrash,faSignOutAlt} from '@fortawesome/free-solid-svg-icons'

library.add(faCheckSquare, faCoffee,faPlus,faEdit,faTrash,faSignOutAlt)