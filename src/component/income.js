import { Modal, Form, Container, Row, Col } from "react-bootstrap"
import { useEffect, useState } from 'react'
import './fontAwesome'

import './../App.css'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';


export default function Income({ setIncTotal }) {
  const [addIncome, setAddIncome] = useState({
    category: ""
  })
  const [isLoading, setIsloading] = useState(false)
  const [inc, setInc] = useState([])
  const [addButtonIs, setAddButtonIs] = useState(false)
  const [buttonIs, setButtonIs] = useState(false)
  const [tol, setTol] = useState({
    total: "",
  })
  const [addcategory, setCategory] = useState({
    category: "",
  })
  const [inIdEntry, setInIdEntry] = useState()
  const [inId, setInId] = useState()
  const [centredModal, setCentredModal] = useState(false);
  const [catModal, setCentredModalAdd] = useState(false);

  const toggleShow = () => setCentredModal(!centredModal)
  const toggleShowAdd = () => setCentredModalAdd(!catModal)
  let allNames = inc?.map(data => {
    return {
      id: data._id,
      name: data.category,
      value: data.value?.map(tot => tot)
    }
  })
  const changeCategory = e => {
    setCategory({
      ...addcategory,
      [e.target.id]: e.target.value
    })
  }
  const changeTotal = e => {
    setTol({
      ...tol,
      [e.target.id]: e.target.value
    })
  }
  const handleEdit = (id, event) => {
    event.preventDefault()
    toggleShow()
    setInId(id)
    setButtonIs(true)
    setAddButtonIs(false)
  }
  const handleEditEntry = (id, event, _id) => {
    event.preventDefault()
    toggleShow()
    setInId(id)
    setInIdEntry(_id)
    setButtonIs(false)
    setAddButtonIs(false)
  }
  const saveUpdate = e => {
    setIsloading(false)
    e.preventDefault()
    fetch(`https://blooming-basin-05163.herokuapp.com/api/users/UpdateIncome/${inId}`, {
      method: "PUT",
      body: JSON.stringify(addcategory),
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }

    }).then(res => {
      setIsloading(true)
      console.log(res)
      if (res.status === 200) {
        alert("Succesfully Updtae")
        return res.json()

      } else {
        alert("Invalid Input")
      }
    })
  }
  const saveUpdateEntry = e => {
    setIsloading(false)
    e.preventDefault()
    fetch(`https://blooming-basin-05163.herokuapp.com/api/users/updateIncomeEntry/${inId}/${inIdEntry}`, {
      method: "PUT",
      body: JSON.stringify(tol),
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }

    }).then(res => {
      setIsloading(true)
      console.log(res)
      if (res.status === 200) {
        alert("Succesfully Updtae")
        return res.json()

      } else {
        alert("Invalid Input")
      }
    })
  }
  const deleteCategory = (id, event) => {
    setIsloading(false)
    event.preventDefault()
    fetch(`https://blooming-basin-05163.herokuapp.com/api/users/DeleteIncome/${id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => {
        setIsloading(true)
        res.json()
      })
      .then(data => {
        console.log(data)
      }).catch(err => console.log(err))
  }
  const handleDeleteEntry = (id, event, _id) => {
    setIsloading(false)
    event.preventDefault()
    fetch(`https://blooming-basin-05163.herokuapp.com/api/users/deleteIncomeEntry/${id}/${_id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => {
        setIsloading(true)
        res.json()
      })
      .then(data => {
        console.log(data)
      }).catch(err => console.log(err))
  }
  const handleAdd = (id, event) => {
    event.preventDefault()
    toggleShow()
    setInId(id)
    setAddButtonIs(true)
  }
  const addEntry = (e, id) => {
    setIsloading(false)
    e.preventDefault()
    fetch(`https://blooming-basin-05163.herokuapp.com/api/users/addIncomeEntry/${inId}`, {
      method: "POST",
      body: JSON.stringify(tol),
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }

    }).then(res => {
      setIsloading(true)
      if (res.status === 200) {
        alert("Succesfully Add Entry")
        return res.json()

      } else {
        alert("Invalid Input")
      }


    })
  }
  const handleAddCat = (event) => {
    event.preventDefault()
    toggleShowAdd()
  }
  useEffect(() => {
    fetch('https://blooming-basin-05163.herokuapp.com/api/users', {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .then(data => {
        fetchIncone()

      }).catch(err => console.log(err.message))
  }, [])

  const fetchIncone = () => {
    fetch('https://blooming-basin-05163.herokuapp.com/api/users', {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .then(data => {
        setInc(data.income)

      }).catch(err => console.log(err.message))
  }

  useEffect(() => {
    if (isLoading) {
      fetchIncone()

    }
  }, [isLoading])


  const submitIncome = e => {
    setIsloading(false)
    e.preventDefault()
    fetch('https://blooming-basin-05163.herokuapp.com/api/users/addIncome', {
      method: "POST",
      body: JSON.stringify(addIncome),
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }

    }).then(res => {
      setIsloading(true)

      if (res.status === 200) {
        alert("Succesfully Add Entry")

        return res.json()

      } else {
        alert("Invalid Input")
      }


    })
  }
  const handleChange = e => {
    setAddIncome({
      ...addIncome,
      [e.target.id]: e.target.value

    })
  }
  const useStyles = makeStyles({
    root: {
      minWidth: 275,
      background: "ivory",
      margin: 12,
      borderRadius: 16
      
    },
    bullet: {
      display: 'inlibe-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    
  });

  const classes = useStyles();

  return (
    
    <Grid
    container
    direction="row"
    justify="center"
    alignItems="flex-start"
  >
     <Button variant="contained" color="" onClick={event => handleAddCat(event)}>
        Add Income
      </Button>
      {/* <Form id="formStyle">
    <Form.Group controlId="category">
      <Form.Label id="labelAdd">Add Income Category:</Form.Label>
      <Form.Control type="text" onChange={handleChange}
        value={addIncome.category} style={{width: "20rem"}} />
        <Button className="mt-2" variant="success" type="submit" onClick={submitIncome} >ADD</Button> 
    </Form.Group>
    
  // </Form> */}
      {
        allNames?.map(data => {
          return (
            
            <Card className={classes.root}>
              <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                  Categorie:
                </Typography>
                <Typography variant="h5" component="h2">
                  {data.name}
             
                  <IconButton aria-label="delete" className={classes.margin} onClick={event => handleAdd(data.id, event)}>
                    <AddIcon fontSize="small" />
                  </IconButton>
                  <IconButton aria-label="delete" className={classes.margin} onClick={event => handleEdit(data.id, event)}>
                    <EditIcon fontSize="small" />
                  </IconButton>
                  <IconButton aria-label="delete" className={classes.margin} onClick={event => deleteCategory(data.id, event)}>
                    <DeleteIcon fontSize="small" />
                  </IconButton>

                </Typography>
                <Typography className={classes.pos} color="textSecondary">
                  Value:
                </Typography>
                {data.value?.map(tots => {
                  let entryVal = tots.total
                  let totalEntry = new Intl.NumberFormat().format(entryVal)
                  return (
                    <Typography variant="body2" component="p">
                      ${totalEntry}

                      <IconButton aria-label="delete" className={classes.margin} onClick={event => handleEditEntry(data.id, event, tots._id)}>
                        <EditIcon fontSize="small" />
                      </IconButton>
                      <IconButton aria-label="delete" className={classes.margin} onClick={event => handleDeleteEntry(data.id, event, tots._id)}>
                        <DeleteIcon fontSize="small" />
                      </IconButton>

                    </Typography>
                  )
                })}
              </CardContent>
              <CardActions>

              </CardActions>
            </Card>
          
          )
        }
        )

      }
      <Modal
        show={centredModal}
        getOpenState={(e) => setCentredModal(e)}
        backdrop="static"
        keyboard={false}
        centered
      >
        <Modal.Body>
          <Form>

            {buttonIs ?
              <Form.Group controlId="category">
                <Form.Label>Set Category:</Form.Label>
                <Form.Control
                  type="text"
                  onChange={changeCategory}
                  value={addcategory.category}
                />
              </Form.Group> :
              <Form.Group controlId="total">
                <Form.Label>Set Total:</Form.Label>
                <Form.Control
                  type="text"
                  onChange={changeTotal}
                  value={tol.total}
                />
              </Form.Group>}
          </Form>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" className='btn-close' color='none' onClick={toggleShow} >
            Close
          </Button>

          {addButtonIs ? <Button variant="primary" onClick={addEntry} >ADD</Button> :
            buttonIs ?
              <Button variant="primary" onClick={saveUpdate}>Save</Button>
              : <Button variant="primary" onClick={saveUpdateEntry}>Save</Button>
          }
        </Modal.Footer>
      </Modal>

      <Modal
        show={catModal}
        getOpenState={(e) => setCentredModalAdd(e)}
        backdrop="static"
        keyboard={false}
        centered
      >
        <Modal.Body>
          <Form>

           
              <Form.Group controlId="category">
                <Form.Label>Set Category Name:</Form.Label>
                <Form.Control
                  type="text" onChange={handleChange}
                  value={addIncome.category}
                />
              </Form.Group> 
          </Form>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" className='btn-close' color='none' onClick={toggleShowAdd} >
            Close
          </Button>
        
         <Button variant="primary" onClick={submitIncome} >ADD</Button> 
           
        </Modal.Footer>
      </Modal>
  
     
      </Grid>

  )
}