import {  useEffect,useState} from 'react'
import { Container, Row, Col } from 'react-bootstrap'
export default function Total(){
  let Inctotals = 0
  let Exptotals = 0
  const [inc, setInc] = useState([])
  const [exp, setExp] = useState([])
  let totalInc = inc?.map(data => data.value.map(tols => tols.total))
  if(totalInc.length > 0){
    let totalsInc = totalInc.flat().reduce((a,b) => a + b,0)
    let finalTotalInc = new Intl.NumberFormat().format(totalsInc)
    Inctotals = finalTotalInc
    
  }
  let totalExp = exp?.map(data => data.value.map(tols => tols.total))
  if(totalExp.length > 0){
    let totalsExp = totalExp.flat().reduce((a,b) => a + b,0)
    let finalTotalExp = new Intl.NumberFormat().format(totalsExp)
    Exptotals = finalTotalExp
    
  }
  useEffect(() => {
    fetch('https://blooming-basin-05163.herokuapp.com/api/users', {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .then(data => {
      setInc(data.income)
      setExp(data.expense)
      }).catch(err => console.log(err.message))
  }, [])


  return(
    <div id="totalStyle">
      <Container>
      <Row >
        <Col className="col-12 col-md-6 " style={{wordWrap: "break-word"}}><h2> Income: {Inctotals}</h2></Col>
        <Col className="col-12 col-md-6" style={{wordWrap: "break-word"}}><h2> Expense: {Exptotals}</h2></Col>
      </Row>
      </Container>
    </div>  
  )
}