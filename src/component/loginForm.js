import { useState, useContext } from 'react'
import { ApplicationContext } from '../context/ApplicationContext'
import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
const useStyles = makeStyles((theme) => ({
	paper: {
		margin: theme.spacing(8, 4),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

export default function LoginForm({setIsRedirect}) {
	const [emailError,setemailError] = useState(false)
	const [passError, setPassError] = useState(false)
	const [error ,setError] = useState(false)
	const { setUser } = useContext(ApplicationContext)
	const [ credentials, setCredentials ] = useState({
		email: "",
		password: ""
	})
	const [ isLoading, setIsLoading ] =useState(false)
	const handleSubmit = e =>{
		e.preventDefault()

		if(credentials.email == ''){
			setemailError(true)
		}
		if(credentials.password == ''){
			setPassError(true)
		}
		else {
		setIsLoading(true)
		fetch('https://blooming-basin-05163.herokuapp.com/api/users/login',{
			method: "POST",
			body: JSON.stringify(credentials),
			headers: {
				"Content-Type" : "application/json"
			}
		})
		.then( res => {
			console.log(res)
			setIsLoading(false)
			if (res.status === 200) {
				alert("Login successful")
				return res.json()
			} else {
				setError(true)
				throw new Error("Invalid Credentials")
			}
		})
		.then( token => {
			let access = token.access
			console.log(access)
      localStorage.setItem("token",access)
			return fetch('https://blooming-basin-05163.herokuapp.com/api/users',{
				headers : {
					'Authorization' : `Bearer ${access}`
				}
			})
		})
		.then( res => res.json())
		.then( data =>{
			const expense = data.expense.map(data => data)
			const income = data.income.map(data => data)
			const { firstName, lastName, email, isAdmin} = data
			setUser({
				userId: data._id,
				firstName,
				lastName,
				email,
				isAdmin,
				expense,
				income
			})
      setIsRedirect(true)
		})
		.catch( err => console.log(err))
	}
}

	const handleChange = e =>{
		setemailError(false)
		setPassError(false)
		setError(false)
		setCredentials({
			
			...credentials,
			[e.target.id] : e.target.value
		})
	}

		const classes = useStyles();
	return(

		<div className={classes.paper}>
		<Typography component="h1" variant="h5">
			Sign in
		</Typography>
		<form className={classes.form} onSubmit={handleSubmit} noValidate>
			<TextField
				variant="outlined"
				margin="normal"
				required
				fullWidth
				id="email"
				onChange={handleChange}
				value={credentials.email}
				label="Email Address"
				name="email"
				type="email"
				autoComplete="email"
				autoFocus
				error={emailError}
				
			/>
			<TextField
				variant="outlined"
				margin="normal"
				required
				fullWidth
				name="password"
				label="Password"
				type="password"
				id="password"
				autoComplete="current-password"
				onChange={handleChange}
				value={credentials.password}
				error ={passError}
			/>
			{error?
			<Alert severity="error">Invalid Email or Password</Alert> : ''
			}
			<Button
				type="submit"
				fullWidth
				variant="contained"
				color="primary"
				className={classes.submit}
			>
				Sign In
			</Button>				
				<Grid item>					
				</Grid>
		</form>
	</div>
	)
}
