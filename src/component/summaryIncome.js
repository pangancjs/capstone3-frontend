import React from 'react';
import {Container, Row, Col } from "react-bootstrap"
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useEffect, useState } from 'react'
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginBottom: 20

  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));



export default function SimpleAccordion() {
  const [inc, setInc] = useState([])
  const [exnp, setExnp] = useState([])
  let allNames = inc?.map(data => {
    return {
      id: data._id,
      name: data.category,
      value: data.value?.map(tot => tot)
    }
  })
  let allNamess = exnp?.map(data => {
    return {
      id: data._id,
      name: data.category,
      value: data.value?.map(tot => tot)
    }
  })

  useEffect(() => {
    fetch('https://blooming-basin-05163.herokuapp.com/api/users', {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .then(data => {
        setInc(data.income)
        setExnp(data.expense)
      }).catch(err => console.log(err.message))
  }, [])
  const classes = useStyles();

  return (
    <>
    <Container>
      <Row>
        <Col className="col-12 col-md-6">
       
     
    <div className={classes.root}>
      <h3>Income</h3>
      {allNames?.map(data => {
        return (
          <Accordion defaultExpanded="true" className={classes.accStyle}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>{data.name}</Typography>
            </AccordionSummary>
            
            {data.value?.map(tots => {
                  let entryVal = tots.total
                  let totalEntry = new Intl.NumberFormat().format(entryVal)
                  return (
                    <AccordionDetails>
                   <h2>${totalEntry}</h2>
                   </AccordionDetails>
                  )
                })}
            
          </Accordion>
        )
      })}
    </div>
    </Col>
    <Col className="col-12 col-md-6">
   <div className={classes.root}>
     <h3>Expense</h3>
     {allNamess?.map(data => {
       return (
         <Accordion  defaultExpanded="true">
           <AccordionSummary
             expandIcon={<ExpandMoreIcon />}
             aria-controls="panel1a-content"
             id="panel1a-header"
           >
             <Typography className={classes.heading}>{data.name}</Typography>
           </AccordionSummary>
           
           {data.value?.map(tots => {
                 let entryVal = tots.total
                 let totalEntry = new Intl.NumberFormat().format(entryVal)
                 return (
                   <AccordionDetails>
                  <h2>${totalEntry}</h2>
                  </AccordionDetails>
                 )
               })}
           
         </Accordion>
       )
     })}
     
   </div>
   </Col>
   </Row>
    </Container>
   
   </>
  )
}
