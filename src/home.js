import Drawer from './pages/Drawer'
import Front from './pages/Front'
import './App.css'
import {useState, useEffect } from 'react'

export default function Home() {
  const checkSession = localStorage.getItem('token')? <Drawer/> : <Front/>
  return (
  <>
  {checkSession}
  </>
  )
}