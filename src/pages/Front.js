import Drawer from './../pages/Drawer'
import "./../App.css"
import Login from "./login"
import LoginForm from './../component/loginForm'
import Register from "./register"
import { Container, Row, Col } from "react-bootstrap"
import { useState } from "react"
import { Redirect } from "react-router-dom"
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
export default function Front() {
  const [isRedirect, setIsRedirect] = useState(false)

  const [logChoice, setLogChoice] = useState(true)
  const loginCall = (event) => {
    event.preventDefault()
    setLogChoice(false)
  }
  const registerCall = (event) => {
    event.preventDefault()
    setLogChoice(true)
  }
  const useStyles = makeStyles((theme) => ({
    root: {
      height: '100vh',
    },
    image: {
      backgroundImage: 'url("https://images.pexels.com/photos/6289065/pexels-photo-6289065.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")',
      backgroundRepeat: 'no-repeat',
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  }));
  const classes = useStyles();
  return (
    isRedirect ?
      <Drawer /> :
      

        <Grid container component="main" className={classes.root}>
          <CssBaseline />
          <Grid item xs={false} sm={4} md={7} className={classes.image} />
          <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          {
            logChoice ?
              <>
                <LoginForm setIsRedirect={setIsRedirect} />
                <label className="d-flex justify-content-center mb-5">
                  Don't have an account? 
                  <a href="" onClick={loginCall}>Sign Up</a>
                </label>
              </>
              :
              <>
                <Register />
                <label className="d-flex justify-content-center">already have account?
                  <a href="" onClick={registerCall}>Login here</a>
                </label>
              </>
          }
          </Grid>
        </Grid>
        
    
  )
}