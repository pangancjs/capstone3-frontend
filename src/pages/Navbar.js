import { Navbar, Nav } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import { useContext } from 'react';
import { ApplicationContext } from './../context/ApplicationContext'
import './../component/fontAwesome'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


export default function MainNav() {
    const history = useHistory()
    const { user, setUser } = useContext(ApplicationContext)
    const handleClick = () => {
        setUser(
            {
                userId: "",
                isAdmin: false,
                email: "",
                firstName: "",
                lastName: "",
                income:"",
                expense:""
            }
        )
        localStorage.clear()
        
        history.push("/")
        window.location.reload()
    }

    const navLinks = !user.userId ?
        <>
        
        </> :
        <>
            <Navbar bg="light" expand="md">
                <Navbar.Brand as={Link} to="/home">Welcome {user.firstName}</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        <Nav.Link onClick={handleClick}>LogOut <FontAwesomeIcon icon="sign-out-alt" size="1x"/></Nav.Link>
                    </Nav>

                </Navbar.Collapse>
            </Navbar>
        </>

    return (
        <div>
            {navLinks}
        </div>

    )
}