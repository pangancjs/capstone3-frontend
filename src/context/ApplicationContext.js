import { createContext, useState, useEffect } from 'react'
export const ApplicationContext = createContext()

export default function ApplicationProvider(props) {

  const [user, setUser] = useState({
    userId: "",
    isAdmin: false,
    email: "",
    firstName: "",
    lastName: "",
    expense: [],
    income: []
  })

  useEffect(() => {
    fetch('https://blooming-basin-05163.herokuapp.com/api/users', {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .then(data => {
        
        let { firstName, lastName, email, isAdmin, income, expense } = data
        setUser({
          userId: data._id,
          firstName: data.firstName,
          lastName: data.lastName,
          email,
          isAdmin,
          income,
          expense
     
        })
      }).catch(err => console.log(err.message))
  }, [])
  return (
    <ApplicationContext.Provider value={{
      setUser,
      user
    }}>
      {props.children}
    </ApplicationContext.Provider>
  )
}