import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import ApplicationProvider from './context/ApplicationContext';
import Home from './home'


function App() {


return (
    <div >
      <ApplicationProvider>
      <Router>
           <Home/>
      </Router>
      </ApplicationProvider>
    </div>
  )
}

export default App;
